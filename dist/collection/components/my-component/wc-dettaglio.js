export class MyComponent {
    constructor() {
        this.urlMappa = "https://www.google.com/maps/search/?api=1&query=";
    }
    OrarioEvento() {
        if (this.orario !== "") {
            return [
                h("div", { class: "info" },
                    h("svg", { viewBox: "0 0 24 24" },
                        h("path", { fill: "none", d: "M0 0h24v24H0V0z" }),
                        h("path", { d: "M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm.5-13H11v6l5.25 3.15.75-1.23-4.5-2.67z" })),
                    h("p", null,
                        "ORARIO: ",
                        this.orario))
            ];
        }
        return;
    }
    DataEvento() {
        if (this.dataevento !== "") {
            return [
                h("div", { class: "info" },
                    h("svg", { viewBox: "0 0 24 24" },
                        h("path", { fill: "none", d: "M0 0h24v24H0V0z" }),
                        h("path", { d: "M20 3h-1V1h-2v2H7V1H5v2H4c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 18H4V10h16v11zm0-13H4V5h16v3z" })),
                    h("p", null,
                        "DATA EVENTO: ",
                        this.dataevento))
            ];
        }
        return;
    }
    VisualizzaMappa(e) {
        e = e;
        window.open(encodeURI(this.urlMappa), '_system');
    }
    render() {
        this.urlMappa += this.luogo;
        return [
            h("div", { class: "component" },
                h("div", null,
                    h("img", { src: this.img }),
                    h("p", { id: "infoPubblicazione" },
                        "Pubblicato il: ",
                        this.datapubblicazione)),
                h("h2", null, this.titolo),
                this.DataEvento(),
                this.OrarioEvento(),
                h("div", { class: "info", id: "mappa", onClick: (e) => this.VisualizzaMappa(e) },
                    h("svg", { viewBox: "0 0 24 24" },
                        h("path", { fill: "none", d: "M0 0h24v24H0V0z" }),
                        h("path", { d: "M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zM7 9c0-2.76 2.24-5 5-5s5 2.24 5 5c0 2.88-2.88 7.19-5 9.88C9.92 16.21 7 11.85 7 9z" }),
                        h("circle", { cx: "12", cy: "9", r: "2.5" })),
                    h("p", null,
                        "LUOGO: ",
                        this.luogo)),
                h("div", { class: "descrizione" },
                    h("p", null, this.descrizione)))
        ];
    }
    static get is() { return "wc-dettaglio"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "dataevento": {
            "type": String,
            "attr": "dataevento"
        },
        "datapubblicazione": {
            "type": String,
            "attr": "datapubblicazione"
        },
        "descrizione": {
            "type": String,
            "attr": "descrizione"
        },
        "durata": {
            "type": String,
            "attr": "durata"
        },
        "img": {
            "type": String,
            "attr": "img"
        },
        "luogo": {
            "type": String,
            "attr": "luogo"
        },
        "orario": {
            "type": String,
            "attr": "orario"
        },
        "titolo": {
            "type": String,
            "attr": "titolo"
        }
    }; }
    static get style() { return "/**style-placeholder:wc-dettaglio:**/"; }
}
