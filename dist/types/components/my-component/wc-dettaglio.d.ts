import '../../stencil.core';
export declare class MyComponent {
    titolo: string;
    img: string;
    datapubblicazione: string;
    dataevento: string;
    orario: string;
    durata: string;
    descrizione: string;
    luogo: string;
    urlMappa: string;
    OrarioEvento(): JSX.Element[];
    DataEvento(): JSX.Element[];
    VisualizzaMappa(e: any): void;
    render(): JSX.Element[];
}
