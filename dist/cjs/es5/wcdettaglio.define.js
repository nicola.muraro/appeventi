"use strict";
// wcdettaglio: Custom Elements Define Library, ES Module/es5 Target
Object.defineProperty(exports, "__esModule", { value: true });
var wcdettaglio_core_js_1 = require("./wcdettaglio.core.js");
var wcdettaglio_components_js_1 = require("./wcdettaglio.components.js");
function defineCustomElements(win, opts) {
    return wcdettaglio_core_js_1.defineCustomElement(win, wcdettaglio_components_js_1.COMPONENTS, opts);
}
exports.defineCustomElements = defineCustomElements;
