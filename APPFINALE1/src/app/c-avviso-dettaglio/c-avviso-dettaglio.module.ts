import { NgModule, ModuleWithProviders, APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { CAvvisoDettaglioComponent } from './c-avviso-dettaglio.component';
import { appInitialize } from '../home/app-initialize';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: CAvvisoDettaglioComponent
      }
    ])
  ],
  declarations: [CAvvisoDettaglioComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CAvvisoDettaglioModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CAvvisoDettaglioModule,
      providers: [
        {
          provide: APP_INITIALIZER,
          useFactory: appInitialize, // fa crashare
          multi: true
        }
      ]
    };
  }
}
