import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ScambioDatiService } from '../services/scambioDati-service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-c-avviso-dettaglio',
  templateUrl: './c-avviso-dettaglio.component.html',
  styleUrls: ['./c-avviso-dettaglio.component.scss'],
})

export class CAvvisoDettaglioComponent implements OnInit {

  //variabili per utilizzare i dati
  dati: any;
  id: string;

  //campi che vengono utilizzati dalla componente stencil
  titolo: string;
  datapubblicazione: string;
  orario: string;
  dataevento = "";
  img: string;
  descrizione;
  luogo: string;

  constructor(private route: ActivatedRoute, private scambioDati: ScambioDatiService, private navigazione: NavController) { }

  ngOnInit() {
    if (this.scambioDati.getDatiCondivisi() == undefined)
      this.navigazione.navigateBack('home');
    else {
      this.id = this.route.snapshot.paramMap.get('id'); //prende un parametro
      let allData = this.scambioDati.getDatiCondivisi();

      let parser = new DOMParser();
      
      this.getDato(allData, this.id);
      console.log(this.dati);
      this.titolo = this.dati.title;
      if (this.datapubblicazione != "")
        this.datapubblicazione = this.formattaData(this.dati.created);
      if (this.dataevento != "")
        this.dataevento = this.formattaData(this.dati.eventDate);
      this.orario = this.dati.eventTiming;
      this.img = this.dati.image;
      this.luogo = this.dati.address;
      this.descrizione = parser.parseFromString(this.dati.description, 'text/html').body.textContent;
    }

  }

  getDato(dati: any, id: string) {

    let i: number = 0;
    let trovato: boolean = false;
    while (!trovato && i < dati.length) {
      if (dati[i].id == id) {
        this.dati = dati[i];
        trovato = true;
      }
      i++;
    }
  }

  formattaData(data): string {
    let anno: string = data.slice(0, 4);
    let mese: string = data.slice(4, 6);
    let giorno: string = data.slice(6, 8);
    return giorno + "/" + mese + "/" + anno;
  }


  Indietro() {
    this.navigazione.pop();
  }

}