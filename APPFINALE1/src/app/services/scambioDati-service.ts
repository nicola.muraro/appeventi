import { Injectable } from '@angular/core';

@Injectable({
   providedIn: 'root'
})

export class ScambioDatiService {

   private datiCondivisi: any; //da utilizzare TipoEvento

   constructor() { }

   setDatiCondivisi(dati: any) { //da utilizzare TipoEvento
      this.datiCondivisi = dati;
      console.log(this.datiCondivisi);
   }

   getDatiCondivisi() {
      console.log(this.datiCondivisi);
      return this.datiCondivisi;
   }
}
