import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})

export class DatiServiceService {

  constructor(private http: Http) { }

  getDati() {
    let vetDati = [];
    let url = 'https://tn.smartcommunitylab.it/trentorienta/api/events';

    let strData = this.http.post(url, {
      "fromDate": null,
      "size": 11,
      "sortForList": 0,
      "source": ["Avvisi del Comune di Trento"],
      "start": 0,
      "tag": [],
      "themes": []
    });

    strData.subscribe(data => {
      for (let item of JSON.parse(data['_body']).content) {
        vetDati.push(item);
      }
    }, error => {
      console.error(error);
    });
    return vetDati;
  }
}
