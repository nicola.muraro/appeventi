import { Component } from '@angular/core';
import { DatiServiceService } from '../services/dati-service.service';
import { TipoEvento } from '../domain/tipo-evento';
import { ScambioDatiService } from '../services/scambioDati-service';

@Component({
  selector: 'home-page',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  isRicercaOpen: boolean = false;
  DatiEvento: TipoEvento[];
  name: string;
  datiRicerca: TipoEvento[] = [];
  constructor(private datiService: DatiServiceService, private scambioDati: ScambioDatiService) { }

  ngOnInit() {
    this.DatiEvento = this.datiService.getDati();
    console.log(this.DatiEvento);
    this.datiRicerca = this.DatiEvento;
    this.scambioDati.setDatiCondivisi(this.datiRicerca);
  }

  OpenCloseRicerca() {
    if (!this.isRicercaOpen) {
      document.getElementById("barraDiRicerca").style.display = "inherit";
      document.getElementById("btnImpostazioniRicerca").innerHTML = "<svg width='24' height='24' viewBox='0 0 24 24'><path fill='none' d='M0 0h24v24H0V0z'/><path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z'/></svg>";
    }
    else {
      document.getElementById("barraDiRicerca").style.display = "none";
      document.getElementById("btnImpostazioniRicerca").innerHTML = "<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'><path fill='none' d='M0 0h24v24H0V0z'/><path d='M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z'/></svg>";
      this.datiRicerca = this.DatiEvento;
    }
    this.isRicercaOpen = !this.isRicercaOpen;
  }

  Ricerca() {
    let vetRisultati: any[] = [];
    if (this.name != null) {
      for (let item of this.DatiEvento) {
        if ((item.description.toUpperCase().includes(this.name.toUpperCase())) || (item.title.toUpperCase().includes(this.name.toUpperCase()))) {
          vetRisultati.push(item);
        }
      }
      this.datiRicerca = vetRisultati;
    }
  }

  formattaData(data): string {
    let anno: string = data.slice(0, 4);
    let mese: string = data.slice(4, 6);
    let giorno: string = data.slice(6, 8);
    return giorno + "/" + mese + "/" + anno;
  }
}
