//import { defineCustomElements as WcAnteprima } from 'wc-anteprima/loader';
import { defineCustomElements as wcAnteprima } from 'wc-anteprima/dist/loader';
import { defineCustomElements as wcDettaglio } from 'wcdettaglio/dist/loader';


export function appInitialize() {
  return () => {
    const win = window as any;
    if (typeof win !== 'undefined') {
      // Define all of our custom elements
      //WALoader(win);
      wcAnteprima(win);
      wcDettaglio(win);
    }
  };
}

